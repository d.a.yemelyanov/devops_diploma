variable "region" {
	default = "eu-central-1"
}

variable "instance_type" {
	default = "t2.micro"
}

variable "key_name" {
    default = "emelianov_public_key"
}

variable "cred_file" {
    default = "$HOME/.aws/credentials"
}

variable "profile" {
    default = "diploma"
}

variable "dns_zone" {
    default = "demokom.tk."
}