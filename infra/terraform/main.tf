# Будем разворачивать наш проект в AWS
provider "aws" {
  region                  = var.region
  shared_credentials_file = var.cred_file
  profile                 = var.profile
}

# Узнаём, какие есть Дата центры в выбранном регионе. Это нужно для Load Balancer.
data "aws_availability_zones" "available" {}

# Получаем образ с последней версией Ubuntu
data "aws_ami" "ubuntu" {
  owners      = ["099720109477"]
  most_recent = true
  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }
}

# Создаем правило, которое будет разрешать трафик к нашим хостам
resource "aws_security_group" "srv" {
  name = "Srv Security Group"

  dynamic "ingress" {
    # Зададим правило, по каким портам можно обращаться к нашим серверам
    for_each = ["80", "22", "8080"]
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name  = "Access to Instances"
  }
}

# Создаем Load Balancer 
resource "aws_elb" "my_lb" {
  name               = "App-Highly-Available-ELB"
  # Бэкенд сервер один, будет достаточно одной зоны. Опция обязательна.
  availability_zones = [data.aws_availability_zones.available.names[0]]
  security_groups    = [aws_security_group.srv.id]
  # LB слушает на порту 80
  listener {
    lb_port           = 80
    lb_protocol       = "http"
    instance_port     = 8080
    instance_protocol = "http"
  }
  # Сделаем интервал проверки поменьше, иначе наш app_server найдется не сразу.
  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 5
    target              = "TCP:8080"
    interval            = 10
  }
  instances = [aws_instance.my_app_server.id]
  tags = {
    Name = "App-Highly-Available-ELB"
  }
}

# Получаем информацию о нашей DNS-зоне
data "aws_route53_zone" "primary" {
  name = var.dns_zone
}

# Создаем Alias "test" для Load Balancer в нашей DNS-зоне
resource "aws_route53_record" "alias_for_my_lb" {
  zone_id = data.aws_route53_zone.primary.zone_id
  name    = "test.${var.dns_zone}"
  type    = "A"

  alias {
    name                   = "${aws_elb.my_lb.dns_name}"
    zone_id                = "${aws_elb.my_lb.zone_id}"
    evaluate_target_health = true
  }
}

# Создаем инстанс сервера приложений, на который будет нацелен LB 
resource "aws_instance" "my_app_server" {
  # Используем образ ubuntu 20.04
  ami             = data.aws_ami.ubuntu.id
  # Возьмем инстанс micro
  instance_type   = var.instance_type
  # Откроем внешние порты 22 и 80
  vpc_security_group_ids = [aws_security_group.srv.id]
  # Значение опционально, но LB скорее всего не найдет наш app_server, для верности укажем зону доступности
  availability_zone = data.aws_availability_zones.available.names[0]
  # какие следует запустить скрипты при создании сервера
  # user_data       = file("install_nginx.sh")
  # Зададим SSH ключ
  key_name = var.key_name
  tags = {
    Name  = "my_app_server"
  } 
  # Если мы решим обновить инстанс, то прежде, чем удалится старый инстанс, должен запуститься новый
  lifecycle {
    create_before_destroy = true
  }
}
